<?php

include_once "bd.inc.php";
include_once "bd.utilisateur.inc.php";

function getCommentaires($idR)
{
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from commentaires where idR=:idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }

    return $resultat;
}

function writeCommentaire($idR, $idU, $text)
{
    try {
        $cnx = connexionPDO();

        $req = $cnx->prepare("insert into commentaires (idU, idR, comm) values(:idU,:idR,:comm)");
        $req->bindValue(':idU', $idU, PDO::PARAM_STR);
        $req->bindValue(':idR', $idR, PDO::PARAM_STR);
        $req->bindValue(':comm', $text, PDO::PARAM_STR);
        
        $resultat = $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

