<?php

include_once "bd.inc.php";
include_once "bd.photo.inc.php";

function getRestoByIdR($idR) {
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from resto where idR=:idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getRestos() {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from resto");
        $req->execute();

        $ligne = $req->fetch(PDO::FETCH_ASSOC);
        while ($ligne) {
            $resultat[] = $ligne;
            $ligne = $req->fetch(PDO::FETCH_ASSOC);
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function editResto($idR, $nomR, $numR, $numAdrR, $voieAdrR, $cpR, $villeR, $descR, $TC) {
    try {
        $cnx = connexionPDO();

        $req = $cnx->prepare("UPDATE resto SET nomR = '$nomR', numR = '$numR', numAdrR = '$numAdrR', voieAdrR = '$voieAdrR', cpR = '$cpR', villeR = '$villeR', descR = '$descR', TC = '$TC' WHERE idR = '$idR' ");

        $resultat = $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

    function addResto( $nomR, $numR, $numAdrR, $voieAdrR, $cpR, $villeR, $descR, $TC, $nomPhoto) {
        try {
            $cnx = connexionPDO();

            $req = $cnx->prepare("INSERT INTO resto (nomR, numR, numAdrR, voieAdrR, cpR, villeR, descR, TC) values(:nomR, :numR, :numAdrR, :voieAdrR, :cpR, :villeR, :descR, :TC); SELECT LAST_INSERT_ID(); " );
            $req->bindValue(':nomR',$nomR,PDO::PARAM_STR);
            $req->bindValue(':numR',$numR,PDO::PARAM_STR);
            $req->bindValue(':numAdrR',$numAdrR,PDO::PARAM_STR);
            $req->bindValue(':voieAdrR',$voieAdrR,PDO::PARAM_STR);
            $req->bindValue(':cpR',$cpR,PDO::PARAM_STR);
            $req->bindValue(':villeR',$villeR,PDO::PARAM_STR);
            $req->bindValue(':descR',$descR,PDO::PARAM_STR);
            $req->bindValue(':TC',$TC,PDO::PARAM_STR);

            $req->execute();
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage();
            die();
        }
        addPhoto($cnx->lastInsertId(), $nomPhoto);
        exit();
      }

      function deleteResto($idR) {
          try {
              $cnx = connexionPDO();

              $req = $cnx->prepare("UPDATE resto SET showR = '0' WHERE idR='$idR'");

              $resultat = $req->execute();
          } catch (PDOException $e) {
              print "Erreur !: " . $e->getMessage();
              die();
          }
          return $resultat;
      }
?>
