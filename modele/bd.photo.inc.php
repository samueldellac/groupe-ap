<?php

include_once "bd.inc.php";

function getPhotosByIdR($idR) {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from photo where idR=:idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function addPhoto( $idR, $nomPhoto) {
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("INSERT into photo (cheminP, idR) values(:cheminP, :idR) " );
        $req->bindValue(':cheminP',$nomPhoto,PDO::PARAM_STR);
        $req->bindValue(':idR',$idR,PDO::PARAM_STR);

        $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}
