<h1>Conditions Générales d'Utilisation</h1>
<h2>Acceptation des conditions générales d'utilisations</h2>

		<p>L'utilisation du site www.r3st0.fr implique l'acceptation des conditions d'utilisations mentionnées ci-dessous.</p>

		<p>Une fois connecté et participant au site, l'utilisateur reconnait avoir lu l'intégralité des conditions générales d'utilisations présentes dans la section CGU du site. </p>

		<p>La société R3sto se réserve le droit de modifier à tout instant les présentes conditions générales. A chaque nouvelle version, les utilisateurs sont avertis par mail, et par un bandeau sur le site.Ainsi les utilisateurs peuvent et doivent se tenir à jour des modifications apportées.<p>

		<p>En cas de manquement aux CGU la société R3sto se réserve le droit de suspendre ou supprimer un compte utilisateur.</p>
		
		<h2>Description du service</h2>

		<p>www.r3st0.fr permet de consulter les fiches des restaurants contenus dans sa base de données. Les restaurants sont décrits à l'aide de caractéristiques précises. Celles-ci permettent d'effectuer des recherches précises. Les utilisateurs ont la possibilité d'émettre un avis personnel sur les repas qu'ils ont pu faire.</p>

		<p>Les restaurants ont la possibilité de <i>répondre</i> aux critiques reçues sur le site. De même les critiques sont relues par une équipe de modération interne à la société R3sto.</p>
		<h2>Fonctionnalités utilisateurs</h2>
		<h3>Conditions générales</h3>
		<p>L'inscription et la publication d'avis ou d'une note est réservée aux personnes <b>majeures</b>.</p>

		<p>Lors de la publication d'un avis l'utilisateur comprend qu'une modération sera effectuée <b>avant</b> que l'avis soit définitivement visible sur le site www.r3st0.fr.</p>

		Sont <b>exclus</b> du droit de publication d'avis :
		<ul>
			<li>les restaurateurs</li>
			<li>les employés de la société R3sto</li>
		</ul>
		
		<p>Les utilisateurs doivent porter un avis objectif sur le restaurant.</p>
		<p>Dans les commentaires peuvent être évalués :</p>
		<ul>
			<li>la qualité gustative</li>
			<li>la propreté</li>
			<li>la diversité des plats</li>
			<li>le cout</li>
			<li>la qualité du service</li>
		</ul>
		<p>Les jugements personnels autres <b>ne peuvent</b> être acceptés. De même il est <b>interdit</b> de faire de la publicité par quelque moyen que ce soit pour un restaurant concurrent dans les commentaires.<p>
		<p>Toute remarque sur l'origine ethnique, culturelle, l'orientation sexuelle, sera modérée, et le compte utilisateur pourra faire l'objet d'une suspension ou d'une suppression.</p>
		
		<h3>Modération des avis</h3>

		<p>Lorsque les avis donnés par les utilisateurs ne respectent pas les règles énoncées dans les CGU, l'avis peut être <b>supprimé, masqué, ou modifié.</b> Une fois modéré, l'utilisateur est averti, et peut dans le cas échéant modifier ou supprimer son commentaire.</p>
		
		<h3>Sanctions prévues en cas de non respect des règles</h3>
		<ol>
			<li>suppression de commentaire</li>
			<li>suspension du compte</li>
			<li>suppression de tous les commentaires déjà postés, et des évaluations.</li>
			<li>suppression du compte</li>
		</ol>
		
		<h3>Liste non exhaustive des motifs de modération des commentaires</h3>
		<ul>
			<li>non respect des règles énoncées dans les CGU du site</li>
			<li>mention spécifique d'une personne</li>
			<li>spam</li>
			<li>commentaire posté par un robot ou généré</li>
			<li>texte illisible</li>
			<li>coordonnées personnelles</li>
			<li>lien vers un site extérieur</li>
			<li>diffamation</li>
			<li>inclusion de code javascript/SQL/PHP ou autre</li>
			<li>informations personnelles (numéro de téléphone, adresse, mail, IP, numéro de carte bancaire...)</li>
		</ul>
		
		<h2>Fonctionnalités restaurateurs</h2>
		<h3>Généralités</h3>
		<h4>Protection des données personnelles</h4>

		<p>Lors de l'inscription certaines données personnelles sont stockées dans le système d'information de R3sto. Ces données sont enregistrées conformément au respect de la protection des données personnelles. Ces données sont utilisées pour permettre à l'utilisateur de profiter de toutes les fonctionnalités du site www.r3st0.fr</p>
		
		<h4>Droits d'accès et de modification</h4>

		<p>Les utilisateurs enregistrés peuvent en accédant à la page "mon compte" modifier les informations personnelles à défaut de leur adresse mail.</p>
		
		<h3>Utilisation de données par des partenaires</h3>

		<p>Lors de l'inscription sur le site, les utilisateurs peuvent accepter ou refuser l'utilisation de leur adresse e-mail à des fins d'information (<i>newsletter</i>) pour le site www.r3st0.fr et d'autres sites de la société R3sto.</p>
		
		<p>Le carnet d'adresse ainsi constitué par la société R3sto peut éventuellement être vendu à des sociétés partenaires, puis utilisé à des fins commerciales pour du démarchage, et pour orienter l'utilisateur vers des produits répondant à ses préférences.</p>
		</br>
