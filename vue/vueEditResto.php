<h1>Modifier le restaurant</h1>
<h4>Modifier le nom du restaurant</h4>
<form action="./?action=applyEditResto" method="POST">
    <input type="text" name="EditnomR" placeholder="NULL" value="<?= $resto['nomR']?>" /><br />

<h4>Modifier la description du restaurant</h4>
    <input type="text" name="EditdescR" placeholder="NULL" value="<?= $resto["descR"]?>" /><br />

<h4>Modifier le numéro de téléphone du restaurant</h4>
    <input type="text" name="EditnumR" placeholder="NULL" value="<?= $resto["numR"]?>" /><br />

<h4>Modifier le numéro de l'adresse du restaurant</h4>
    <input type="text" name="EditnumAdrR" placeholder="NULL" value="<?= $resto["numAdrR"]?>" /><br />

<h4>Modifier la rue du restaurant</h4>
    <input type="text" name="EditvoieAdrR" placeholder="NULL" value="<?= $resto["voieAdrR"]?>" /><br />

<h4>Modifier la ville du restaurant</h4>
    <input type="text" name="EditvilleR" placeholder="NULL" value="<?= $resto["villeR"]?>" /><br />

<h4>Modifier le code postal du restaurant</h4>
    <input type="text" name="EditcpR" placeholder="NULL" value="<?=$resto["cpR"]?>" /><br />

<h4>Modifier le type de cuisine du restaurant</h4>
    <input type="text" name="TC" placeholder="Later" value="" /><br />

    <input type="hidden" name="idR" value="<?=$idR?>" />
    <input type="submit" value="Appliquer les modifications" />
</form>

<h1> Restaurant:</h1>

<?php
  $lesPhotos = getPhotosByIdR($idR);
    ?>

    <div class="card">
    <div class="photoCard">
            <?php if (count($lesPhotos) > 0) { ?>
                <img src="photos/<?= $lesPhotos[0]["cheminP"] ?>" alt="photo du restaurant" />
            <?php } ?>
        </div>

        <div class="descrCard"><?php echo "<a href='./?action=detail&idR=" . $resto['idR'] . "'>" . $resto['nomR'] . "</a>"; ?>
            <br />
            <?php
                if ($resto["numR"] != NULL) echo($resto["numR"]);
                else echo("Tél. non renseigné");
            ?>
            <br />
            <br />
            <?= $resto["numAdrR"] ?>
            <?= $resto["voieAdrR"] ?>
            <br />
            <?= $resto["cpR"] ?>
            <?= $resto["villeR"] ?>
        </div>
