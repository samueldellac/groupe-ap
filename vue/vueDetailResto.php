<h1><?= $unResto['nomR']; ?></h1>

<section>
    Cuisine <br />
	<!--
	<ul id="tagFood">

		<li class="tag">
			<span class="tag">#</span>
		</li>

	</ul>

	-->
</section>
<p id="principal">
    <?= $unResto['descR']; ?>
</p>
<p id="principal">
    <?php if (count($lesPhotos) > 0) { ?>
        <img src="photos/<?= $lesPhotos[0]["cheminP"] ?>" alt="photo du restaurant" width="100%"/>
    <?php } ?>
</p>

<h2 id="telephone">
    Contact
</h2>
<p>
    <?php
        if ($unResto["numR"] != NULL) echo($unResto["numR"]);
        else echo("Aucun numéro de téléhpone renseigné.");
    ?>
</p>

<h2 id="adresse">
    Adresse
</h2>
<p>
    <?= $unResto['numAdrR']; ?>
    <?= $unResto['voieAdrR']; ?><br />
    <?= $unResto['cpR']; ?>
    <?= $unResto['villeR']; ?>
</p>

<h2 id="photos">
    Photos
</h2>
<ul id="galerie">
    <?php for ($i = 0; $i < count($lesPhotos); $i++) { ?>
        <li> <img class="galerie" src="photos/<?= $lesPhotos[$i]["cheminP"] ?>" alt="" width="100%"/></li>
    <?php } ?>

</ul>

</ul>
<h2 id="com"> Ajouter une critique</h2>
<?php
    if (!isLoggedOn()) echo("Vous ne pouvez pas envoyer de critique tant que vous n'êtes pas connecté");
    else 
        echo('
            <form action="./?action=comment" method="POST">
                <input type="text" name="Critique" placeholder="Ajouter une critique"  />
                <input type="hidden" name="idR" value="'.$unResto["idR"].'" />
                <input type="hidden" name="idU" value="'.$_SESSION["idU"].'" />

            </form>
        ')
    ;
?>

<h2 id="crit">Critiques</h2>
<ul id="critiques">

<?php for ($i = 0; $i < count($comments); $i++) { ?>

    <div class="commentCard">

        <div class="commentTextCard">
            <b><?= getUtilisateurByidU($comments[$i]["idU"])["pseudoU"] ?></b>
            <br /><br />
            <?= $comments[$i]["comm"] ?>
        </div>

    </div>
<?php }; ?>
