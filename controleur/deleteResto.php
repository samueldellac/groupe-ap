<?php

include_once "$racine/modele/bd.resto.inc.php";
include_once "$racine/modele/bd.photo.inc.php";

// recuperation des donnees GET, POST, et SESSION
$idR = $_GET["idR"];

$resto=getRestoByIdR($idR);
$titre = "Supprimer un restaurant";
include "$racine/vue/entete.html.php";
include "$racine/vue/vueDeleteResto.php";
include "$racine/vue/pied.html.php";
?>
