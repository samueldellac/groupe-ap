<?php

include_once "$racine/modele/bd.resto.inc.php";
include_once "$racine/modele/bd.photo.inc.php";

// recuperation des donnees GET, POST, et SESSION 
$idR = $_GET["idR"];

$resto=getRestoByIdR($idR);

$titre = "Éditer un restaurant";
include "$racine/vue/entete.html.php";
include "$racine/vue/vueEditResto.php";
include "$racine/vue/pied.html.php";
?>
