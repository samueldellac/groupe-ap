<?php
    include_once "$racine/modele/bd.resto.inc.php";

    if (isset($_POST["AddNom"]) and isset($_POST["AddNRue"])and isset($_POST["AddRue"]) and isset($_POST["AddCPR"])and isset($_POST["AddVille"])and isset($_POST["AddDescR"])){

        $nomR=$_POST["AddNom"];
        $numR=$_POST["AddNum"];
        $numAdrR=$_POST["AddNRue"];
        $voieAdrR=$_POST["AddRue"];
        $cpR=$_POST["AddCPR"];
        $villeR=$_POST["AddVille"];
        $descR=$_POST["AddDescR"];
        $TC="{}";

        $nomOrigine = $_FILES['monfichier']['name'];
        $elementsChemin = pathinfo($nomOrigine);
        $extensionFichier = $elementsChemin['extension'];
        $extensionsAutorisees = array("jpeg", "jpg", "gif", "png", "apng");
        if (!(in_array($extensionFichier, $extensionsAutorisees))) {
            echo "Le fichier n'a pas l'extension attendue";
            exit();
        } else {    
            // Copie dans le repertoire du script avec un nom
            // incluant l'heure a la seconde pres 
            $repertoireDestination = $racine."/photos/";
            $nomDestination = $nomR."-".date("YmdHis").".".$extensionFichier;

            if (move_uploaded_file($_FILES["monfichier"]["tmp_name"], $repertoireDestination.$nomDestination)) {
                addResto( $nomR, $numR, $numAdrR, $voieAdrR, $cpR, $villeR, $descR, $TC, $nomDestination);
                header('Location: ../');

            } else {
                echo "Le fichier n'a pas été uploadé (trop gros ?) ou ".
                        "Le déplacement du fichier temporaire a échoué".
                        " vérifiez l'existence du répertoire ".$repertoireDestination.
                        "\nL'ajout du restaurant a été annulé !";
            }
        }
    }
