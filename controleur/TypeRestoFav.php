<?php

include_once "$racine/modele/bd.resto.inc.php";
include_once "$racine/modele/bd.photo.inc.php";

// recuperation des donnees GET, POST, et SESSION

$titre = "Ajouter un type de cuisine favori";
include "$racine/vue/entete.html.php";
include "$racine/vue/vueTypeCusine.php";
include "$racine/vue/pied.html.php";
?>
