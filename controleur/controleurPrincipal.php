<?php

function controleurPrincipal($action){
    $lesActions = array();
    $lesActions["defaut"] = "listeRestos.php";
	$lesActions["notFound"] = "notFound.php";
    $lesActions["accueil"] = "listeRestos.php";
    $lesActions["detail"] = "detailResto.php";
    $lesActions["connexion"] = "connexion.php";
    $lesActions["deconnexion"] = "deconnexion.php";
    $lesActions["profil"] = "monProfil.php";
    $lesActions["inscription"] = "inscription.php";
    $lesActions["cgu"] = "CGU.php";
    $lesActions["comment"] = "commentaire.php";
	$lesActions["recherche"] = "Recherche.php";
    $lesActions["editResto"] = "editResto.php";
    $lesActions["applyEditResto"] = "applyEditResto.php";
	$lesActions["addResto"] = "addResto.php";
  $lesActions["applyAddResto"] = "applyAddResto.php";
  $lesActions["deleteResto"]="deleteResto.php";
  $lesActions["applyDeleteResto"]="applyDeleteResto.php";
  $lesActions["addTC"]="TypeRestoFav.php";
  $lesActions["applyAddTC"]="applyAddTC.php";

    if (array_key_exists ( $action , $lesActions )){
        return $lesActions[$action];
    }
    else{
        return $lesActions["notFound"];
    }

}

?>
