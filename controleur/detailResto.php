<?php
include_once "$racine/modele/bd.resto.inc.php";
include_once "$racine/modele/bd.photo.inc.php";
include_once "$racine/modele/bd.comment.inc.php";
include_once "$racine/modele/authentification.inc.php";

// recuperation des donnees GET, POST, et SESSION
$idR = $_GET["idR"];

// creation du menu burger
$menuBurger = array();
if(isAdmin()) $menuBurger[] = Array("url"=>"./?action=editResto&idR=".$idR."","label"=>"Editer le restaurant");
if(isAdmin()) $menuBurger[] = Array("url"=>"./?action=deleteResto&idR=".$idR."","label"=>"Supprimer le restaurant");
$menuBurger[] = Array("url"=>"#top","label"=>"Le restaurant");
$menuBurger[] = Array("url"=>"#adresse","label"=>"Adresse");
$menuBurger[] = Array("url"=>"#photos","label"=>"Photos");
$menuBurger[] = Array("url"=>"#crit","label"=>"Critiques");


// appel des fonctions permettant de recuperer les donnees utiles a l'affichage
$unResto = getRestoByIdR($idR);
$lesPhotos = getPhotosByIdR($idR);
$comments = getCommentaires($idR);

// traitement si necessaire des donnees recuperees
;

// appel du script de vue qui permet de gerer l'affichage des donnees
$titre = "detail d'un restaurant";
include "$racine/vue/entete.html.php";
include "$racine/vue/vueDetailResto.php";
include "$racine/vue/pied.html.php";
