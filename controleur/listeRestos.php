<?php
include_once "$racine/modele/bd.resto.inc.php";
include_once "$racine/modele/bd.photo.inc.php";

// recuperation des donnees GET, POST, et SESSION
;

$menuBurger = array();
 $menuBurger[] = Array("url"=>"./?action=addResto&idR=","label"=>"Ajouter un restaurant");
 if(isUser())$menuBurger[] = Array("url"=>"./?action=addTC=","label"=>"Choissisez vos types de cusine favori");
// appel des fonctions permettant de recuperer les donnees utiles a l'affichage
$listeRestos = getRestos();

// traitement si necessaire des donnees recuperees
;

// appel du script de vue qui permet de gerer l'affichage des donnees
$titre = "Liste des restaurants répertoriés";
include "$racine/vue/entete.html.php";
include "$racine/vue/vueListeRestos.php";
include "$racine/vue/pied.html.php";
?>
